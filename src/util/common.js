var isSubmit = false;

const common = (Vue, options)=> {
    /**
     * obj:{
     *     type: '',
     *     message: '操作成功!'
     *     successFn: fn
     * }
     * @param {数据对象} obj 
     */
    Vue.prototype.alertTip = function(obj) {
        this.$message({
            type: obj.type,
            message: obj.message
        });
        setTimeout(()=>{
            obj.successFn();
        }, 1000);
    },

    /**
     * obj:{
     *     dispatchName: ''//this.$store.dispatch调用名称参数，
     *     data: '后台接口数据参数'
     * }
     * fn(data): 回调方法(data请求后台返回的数据)
     * @param {数据对象} obj 
     * @param {回调方法} fn 
     */
    Vue.prototype.submitDispatch = function(obj, fn) {
        if(isSubmit){
            this.alertWarn('重复提交!');
            return;
        }
        isSubmit = true;
        this.$store.dispatch(obj.dispatchName, Object.assign({}, obj.data)).then((data)=>{
            new Promise((resolve, reject)=>{
                fn(data);
                resolve();
            }).then(()=>{
                setTimeout(()=>{
                    isSubmit = false;
                }, 1000);
            });
        });
    },

    Vue.prototype.alertWarn = function(message) {
        this.$store.state.app.dialogFlag.message = message;
        this.$store.state.app.dialogFlag.visible = true;
        // this.$confirm(message, '提示', {
        //     confirmButtonText: '确定',
        //     cancelButtonText: '',
        //     type: 'warning'
        // }).then(() => {});   
    }
};

export default common;