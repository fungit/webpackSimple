import userService from '../../api/modules/user';
import md5 from '../../util/md5Util';

const user = {
    state: {
    },
    mutations: {

    },
    actions: {
        login({commit}, user){
            user.loginName = user.loginName.trim();
            user.password = md5.hex_md5(user.password);
            return userService.login(user).then((result)=>{return result.data});
        },
        register({commit}, userInfo){
            for(var key in userInfo){
                userInfo[key] = userInfo[key].trim();
            }
            userInfo.USER_PASSWORD = md5.hex_md5(userInfo.USER_PASSWORD);
            return userService.register(userInfo).then((result)=>{return result.data});
        }
    }
};

export default user;