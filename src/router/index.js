import Vue from 'vue'
import Router from 'vue-router'
import indexPage from '../components/indexPage'
import login from '../components/login/login'
import register from '../components/login/register'

Vue.use(Router);

const routers = [{
    path: '/',
    component: indexPage
},{
    path: '/login',
    component: login
},{
    path: '/register',
    component: register
}];

export default new Router({
    routes: routers
});