import axiosService from '../common';

const userService = {
    login(user) {
        return axiosService.post('/userInfo!login', user);
    },
    register(userInfo) {
        return axiosService.post('/userInfo!saveUserInfo', userInfo);
    }
}

export default userService;