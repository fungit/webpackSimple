import axios from 'axios';
import Qs from 'qs';

const axiosService = {
    post(url, param){
        return axios({
            url: '/service' + url,
            method: 'post',
            data: param,
            transformRequest: [function (data) {
              data = Qs.stringify(data);
              return data;
            }],
            headers:{'Content-Type':'application/x-www-form-urlencoded; charset=utf-8'}
        });
    }
}

export default axiosService;