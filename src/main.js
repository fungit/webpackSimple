import Vue from 'vue';
import store from './store';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';
import dialog from './components/dialog/dialog.vue';
import 'element-ui/lib/theme-chalk/index.css';
import common from './util/common';

Vue.use(ElementUI);
Vue.use(common);
Vue.component('jhDialog', dialog);

new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
});
